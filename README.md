# logger

Logger used for aethor and various other snipppets for grafana

http logs query

```
sum by (category) (rate({process="Deno"}|json|label="REST" [1m]))
```

```
sum by (category) (count_over_time({process="Deno"}|json|label="REST" [1m]))
```

```
sum by (command) (count_over_time({process="Deno"}|json|text="[command]: started" [1h]))
```

```
sum by (category) (count_over_time({process="Deno"}|json|label="REST" [1m]))
```


```json
{
  "id": 20,
  "gridPos": {
    "h": 16,
    "w": 12,
    "x": 0,
    "y": 17
  },
  "type": "logs",
  "title": "Logs",
  "datasource": {
    "type": "loki",
    "uid": "REDACTED"
  },
  "options": {
    "showTime": false,
    "showLabels": false,
    "showCommonLabels": false,
    "wrapLogMessage": false,
    "prettifyLogMessage": false,
    "enableLogDetails": true,
    "dedupStrategy": "exact",
    "sortOrder": "Descending"
  },
  "targets": [
    {
      "datasource": {
        "type": "loki",
        "uid": "REDACTED"
      },
      "expr": "{process=\"Deno\"}|json|label!=\"REST\"",
      "legendFormat": "",
      "refId": "A"
    }
  ]
}
```

Normal logs query

```
{host="localhost"}
```