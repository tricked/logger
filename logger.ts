import { botName } from "../../../settings.ts";
import { AethorClient } from "../mod.ts";
import {
  blue,
  bold,
  cyan,
  DiscordenoMessage,
  Embed,
  gray,
  green,
  NaticoCommand,
  red,
  yellow,
  magenta,
  brightBlue,
  reset,
  structures,
  rest,
} from "../../../deps.ts";
type Entry = [string, string];

interface Body {
  streams: [
    {
      stream: Record<string, string>;
      values: Entry[];
    }
  ];
}
interface LoggerRecord
  extends Record<string, string | number | Date | Record<string, string> | undefined | bigint | boolean> {
  hidden?: boolean;
}
enum Labels {
  error = "ERROR",
  warn = "WARN",
  debug = "DEBUG",
  info = "INFO",
  rest = "REST",
}
const consoleLabels = {
  [Labels.warn]: "warn",
  [Labels.error]: "error",
  [Labels.info]: "info",
  [Labels.debug]: "debug",
  [Labels.rest]: "debug",
} as const;
const consoleColors = {
  [Labels.warn]: yellow,
  [Labels.error]: red,
  [Labels.info]: green,
  [Labels.debug]: blue,
  [Labels.rest]: brightBlue,
} as const;
/**
 *  Loki logger
 *  @param url  path is where LokiLogger will post to
 *  @param buffer  is the amount of entries to cache before sending
 *  @param labels Labels to use
 *  @param mode mode to post
 */
export class AethorLogger {
  toReport?: Entry[];
  constructor(
    public client: AethorClient,
    public url: string = "http://localhost:3100",
    public buffer = 4,
    public labels: Record<string, string> = {
      host: "localhost",
      job: botName || "Aethor",
      process: "Deno",
    },
    public mode: "JSON" | "TEXT" = "JSON"
  ) {
    if (buffer !== 1) {
      this.toReport = [];
    }
    rest.eventHandlers["status"] = (s: number) => {
      let message;
      let category;
      if (199 < s && s < 300) {
        message = "Ok!";
        category = 200;
      } else if (399 < s && s < 499) {
        category = 400;
        message = "Invalid requests";
      } else if (499 < s && s < 599) {
        category = 500;
        message = "Discord api error";
      }

      this.client.logger.rest(`[rest]: ${message}`, {
        message,
        category,
        status: s,
      });
    };
  }
  private async pushPayload(entry: Entry) {
    if (this.buffer !== 1) this.toReport!.push(entry);
    if (this.buffer === this.toReport?.length) {
      await this.report(this.createPayload(this.toReport));
      //Clear the reports
      this.toReport.length = 0;
    } else if (this.buffer === 1) {
      await this.report(this.createPayload([entry]));
    }
  }
  private createPayload(entries: Entry[]): Body {
    return {
      streams: [{ stream: this.labels, values: entries }],
    };
  }
  private async report(body: Body) {
    //https://github.com/grafana/loki/issues/173
    //https://github.com/sleleko/devops-kb/blob/master/python/push-to-loki.py
    const r = await fetch(`${this.url}/loki/api/v1/push`, {
      headers: { "Content-type": "application/json" },
      method: "POST",
      body: JSON.stringify(body),
    });
    if (!r.ok) {
      try {
        throw new Error(`FAILED TO POST DATA ${await r.text()} ${JSON.stringify(r)}`);
      } catch (e) {
        console.error(e);
      }
    }
  }

  public log(label: Labels, text: string, meta?: LoggerRecord) {
    if (meta)
      Object.keys(meta).forEach(
        (key) =>
          //@ts-ignore -
          (meta[key] === undefined && delete meta[key]) ||
          //@ts-ignore -
          (typeof meta[key] == "bigint" && (meta[key] = meta[key]?.toString()))
      );
    console[consoleLabels[label]](
      `${consoleColors[label](`[${label}]`)} ${text} ${
        meta
          ? Object.entries(meta)
              .filter((x) => x[0] !== "hidden")
              .map(([k, v]) => `${gray(k)}=${gray(v?.toString() || "")}`)
              .join(", ")
          : ""
      }`
    );
    if (meta?.hidden) return;

    meta = {
      label,
      text,
      ...meta,
    };
    return this.pushPayload([
      `${Date.now()}000000`,
      this.mode == "JSON" && meta
        ? JSON.stringify(meta)
        : Object.entries(meta)
            .map(([k, v]) => `${k}="${v}"`)
            .join(" "),
    ]);
  }

  public info(text: string, meta?: LoggerRecord) {
    return this.log(Labels.info, text, meta);
  }
  public warn(text: string, meta?: LoggerRecord) {
    return this.log(Labels.warn, text, meta);
  }
  public error(text: string, meta?: LoggerRecord, error?: any) {
    return Promise.all([
      this.log(Labels.error, text, { ...meta, error }),
      this.client.errorManager.report(error, {
        ...meta,
        text,
      }),
    ]);
  }
  public debug(text: string, meta?: LoggerRecord) {
    return this.log(Labels.debug, text, meta);
  }
  public rest(text: string, meta?: LoggerRecord) {
    return this.log(Labels.rest, text, meta);
  }
}
